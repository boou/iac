type Account {
  id: ID!
  name: String!
  cc: String!
  phone: String!
  credits(filter: ModelCreditFilterInput, sortDirection: ModelSortDirection, limit: Int, nextToken: String): ModelCreditConnection
  loan(filter: ModelLoanFilterInput, sortDirection: ModelSortDirection, limit: Int, nextToken: String): ModelLoanConnection
}

type Credit {
  id: ID!
  loan(filter: ModelLoanFilterInput, sortDirection: ModelSortDirection, limit: Int, nextToken: String): ModelLoanConnection
  name: String!
  numCuotas: Int!
  beginDate: String!
  expireDate: String!
}

type Bank {
  id: ID!
  name: String!
  country: String!
  interest: Float!
}

type Loan {
  id: ID!
  name: Float!
  type: CreditType!
  bank(filter: ModelBankFilterInput, sortDirection: ModelSortDirection, limit: Int, nextToken: String): ModelBankConnection
  franchise: String!
  cutDate: String!
  cuotaManejo: Float!
}

enum CreditType {
  CARD
  CONSUMO
}

enum ModelSortDirection {
  ASC
  DESC
}

type ModelAccountConnection {
  items: [Account]
  nextToken: String
}

input ModelStringFilterInput {
  ne: String
  eq: String
  le: String
  lt: String
  ge: String
  gt: String
  contains: String
  notContains: String
  between: [String]
  beginsWith: String
}

input ModelIDFilterInput {
  ne: ID
  eq: ID
  le: ID
  lt: ID
  ge: ID
  gt: ID
  contains: ID
  notContains: ID
  between: [ID]
  beginsWith: ID
}

input ModelIntFilterInput {
  ne: Int
  eq: Int
  le: Int
  lt: Int
  ge: Int
  gt: Int
  contains: Int
  notContains: Int
  between: [Int]
}

input ModelFloatFilterInput {
  ne: Float
  eq: Float
  le: Float
  lt: Float
  ge: Float
  gt: Float
  contains: Float
  notContains: Float
  between: [Float]
}

input ModelBooleanFilterInput {
  ne: Boolean
  eq: Boolean
}

input ModelAccountFilterInput {
  id: ModelIDFilterInput
  name: ModelStringFilterInput
  cc: ModelStringFilterInput
  phone: ModelStringFilterInput
  and: [ModelAccountFilterInput]
  or: [ModelAccountFilterInput]
  not: ModelAccountFilterInput
}

type Query {
  getAccount(id: ID!): Account
  listAccounts(filter: ModelAccountFilterInput, limit: Int, nextToken: String): ModelAccountConnection
  getCredit(id: ID!): Credit
  listCredits(filter: ModelCreditFilterInput, limit: Int, nextToken: String): ModelCreditConnection
  getBank(id: ID!): Bank
  listBanks(filter: ModelBankFilterInput, limit: Int, nextToken: String): ModelBankConnection
  getLoan(id: ID!): Loan
  listLoans(filter: ModelLoanFilterInput, limit: Int, nextToken: String): ModelLoanConnection
}

input CreateAccountInput {
  id: ID
  name: String!
  cc: String!
  phone: String!
}

input UpdateAccountInput {
  id: ID!
  name: String
  cc: String
  phone: String
}

input DeleteAccountInput {
  id: ID
}

type Mutation {
  createAccount(input: CreateAccountInput!): Account
  updateAccount(input: UpdateAccountInput!): Account
  deleteAccount(input: DeleteAccountInput!): Account
  createCredit(input: CreateCreditInput!): Credit
  updateCredit(input: UpdateCreditInput!): Credit
  deleteCredit(input: DeleteCreditInput!): Credit
  createBank(input: CreateBankInput!): Bank
  updateBank(input: UpdateBankInput!): Bank
  deleteBank(input: DeleteBankInput!): Bank
  createLoan(input: CreateLoanInput!): Loan
  updateLoan(input: UpdateLoanInput!): Loan
  deleteLoan(input: DeleteLoanInput!): Loan
}

type Subscription {
  onCreateAccount: Account @aws_subscribe(mutations: ["createAccount"])
  onUpdateAccount: Account @aws_subscribe(mutations: ["updateAccount"])
  onDeleteAccount: Account @aws_subscribe(mutations: ["deleteAccount"])
  onCreateCredit: Credit @aws_subscribe(mutations: ["createCredit"])
  onUpdateCredit: Credit @aws_subscribe(mutations: ["updateCredit"])
  onDeleteCredit: Credit @aws_subscribe(mutations: ["deleteCredit"])
  onCreateBank: Bank @aws_subscribe(mutations: ["createBank"])
  onUpdateBank: Bank @aws_subscribe(mutations: ["updateBank"])
  onDeleteBank: Bank @aws_subscribe(mutations: ["deleteBank"])
  onCreateLoan: Loan @aws_subscribe(mutations: ["createLoan"])
  onUpdateLoan: Loan @aws_subscribe(mutations: ["updateLoan"])
  onDeleteLoan: Loan @aws_subscribe(mutations: ["deleteLoan"])
}

type ModelCreditConnection {
  items: [Credit]
  nextToken: String
}

input ModelCreditFilterInput {
  id: ModelIDFilterInput
  name: ModelStringFilterInput
  numCuotas: ModelIntFilterInput
  beginDate: ModelStringFilterInput
  expireDate: ModelStringFilterInput
  and: [ModelCreditFilterInput]
  or: [ModelCreditFilterInput]
  not: ModelCreditFilterInput
}

input CreateCreditInput {
  id: ID
  name: String!
  numCuotas: Int!
  beginDate: String!
  expireDate: String!
  accountCreditsId: ID
}

input UpdateCreditInput {
  id: ID!
  name: String
  numCuotas: Int
  beginDate: String
  expireDate: String
  accountCreditsId: ID
}

input DeleteCreditInput {
  id: ID
}

type ModelBankConnection {
  items: [Bank]
  nextToken: String
}

input ModelBankFilterInput {
  id: ModelIDFilterInput
  name: ModelStringFilterInput
  country: ModelStringFilterInput
  interest: ModelFloatFilterInput
  and: [ModelBankFilterInput]
  or: [ModelBankFilterInput]
  not: ModelBankFilterInput
}

input CreateBankInput {
  id: ID
  name: String!
  country: String!
  interest: Float!
  loanBankId: ID
}

input UpdateBankInput {
  id: ID!
  name: String
  country: String
  interest: Float
  loanBankId: ID
}

input DeleteBankInput {
  id: ID
}

type ModelLoanConnection {
  items: [Loan]
  nextToken: String
}

input ModelCreditTypeFilterInput {
  eq: CreditType
  ne: CreditType
}

input ModelLoanFilterInput {
  id: ModelIDFilterInput
  name: ModelFloatFilterInput
  type: ModelCreditTypeFilterInput
  franchise: ModelStringFilterInput
  cutDate: ModelStringFilterInput
  cuotaManejo: ModelFloatFilterInput
  and: [ModelLoanFilterInput]
  or: [ModelLoanFilterInput]
  not: ModelLoanFilterInput
}

input CreateLoanInput {
  id: ID
  name: Float!
  type: CreditType!
  franchise: String!
  cutDate: String!
  cuotaManejo: Float!
  accountLoanId: ID
  creditLoanId: ID
}

input UpdateLoanInput {
  id: ID!
  name: Float
  type: CreditType
  franchise: String
  cutDate: String
  cuotaManejo: Float
  accountLoanId: ID
  creditLoanId: ID
}

input DeleteLoanInput {
  id: ID
}
