{
    "AWSTemplateFormatVersion": "2010-09-09",
    "Description": "An auto-generated nested stack.",
    "Metadata": {},
    "Parameters": {
        "AppSyncApiName": {
            "Type": "String",
            "Description": "The name of the AppSync API",
            "Default": "AppSyncSimpleTransform"
        },
        "APIKeyExpirationEpoch": {
            "Type": "Number",
            "Description": "The epoch time in seconds when the API Key should expire. Setting this to 0 will default to 1 week from the deployment date. Setting this to -1 will not create an API Key.",
            "Default": 0
        },
        "DynamoDBModelTableReadIOPS": {
            "Type": "Number",
            "Description": "The number of read IOPS the table should support.",
            "Default": 5
        },
        "DynamoDBModelTableWriteIOPS": {
            "Type": "Number",
            "Description": "The number of write IOPS the table should support.",
            "Default": 5
        },
        "DynamoDBBillingMode": {
            "Type": "String",
            "Description": "Configure @model types to create DynamoDB tables with PAY_PER_REQUEST or PROVISIONED billing modes.",
            "Default": "PAY_PER_REQUEST",
            "AllowedValues": [
                "PAY_PER_REQUEST",
                "PROVISIONED"
            ]
        },
        "DynamoDBEnablePointInTimeRecovery": {
            "Type": "String",
            "Description": "Whether to enable Point in Time Recovery on the table",
            "Default": "false",
            "AllowedValues": [
                "true",
                "false"
            ]
        },
        "AuthCognitoUserPoolId": {
            "Type": "String",
            "Description": "The id of an existing User Pool to connect. If this is changed, a user pool will not be created for you.",
            "Default": "NONE"
        },
        "env": {
            "Type": "String",
            "Description": "The environment name. e.g. Dev, Test, or Production",
            "Default": "NONE"
        },
        "S3DeploymentBucket": {
            "Type": "String",
            "Description": "The S3 bucket containing all deployment assets for the project."
        },
        "S3DeploymentRootKey": {
            "Type": "String",
            "Description": "An S3 key relative to the S3DeploymentBucket that points to the root of the deployment directory."
        },
        "AppSyncApiId": {
            "Type": "String",
            "Description": "The id of the AppSync API associated with this project."
        },
        "GetAttGraphQLAPIApiId": {
            "Type": "String",
            "Description": "Auto-generated parameter that forwards Fn.GetAtt(GraphQLAPI, ApiId) through to nested stacks."
        }
    },
    "Resources": {
        "AccountTable": {
            "Type": "AWS::DynamoDB::Table",
            "Properties": {
                "TableName": {
                    "Fn::If": [
                        "HasEnvironmentParameter",
                        {
                            "Fn::Join": [
                                "-",
                                [
                                    "Account",
                                    {
                                        "Ref": "GetAttGraphQLAPIApiId"
                                    },
                                    {
                                        "Ref": "env"
                                    }
                                ]
                            ]
                        },
                        {
                            "Fn::Join": [
                                "-",
                                [
                                    "Account",
                                    {
                                        "Ref": "GetAttGraphQLAPIApiId"
                                    }
                                ]
                            ]
                        }
                    ]
                },
                "KeySchema": [
                    {
                        "AttributeName": "id",
                        "KeyType": "HASH"
                    }
                ],
                "AttributeDefinitions": [
                    {
                        "AttributeName": "id",
                        "AttributeType": "S"
                    }
                ],
                "StreamSpecification": {
                    "StreamViewType": "NEW_AND_OLD_IMAGES"
                },
                "BillingMode": {
                    "Fn::If": [
                        "ShouldUsePayPerRequestBilling",
                        "PAY_PER_REQUEST",
                        {
                            "Ref": "AWS::NoValue"
                        }
                    ]
                },
                "ProvisionedThroughput": {
                    "Fn::If": [
                        "ShouldUsePayPerRequestBilling",
                        {
                            "Ref": "AWS::NoValue"
                        },
                        {
                            "ReadCapacityUnits": {
                                "Ref": "DynamoDBModelTableReadIOPS"
                            },
                            "WriteCapacityUnits": {
                                "Ref": "DynamoDBModelTableWriteIOPS"
                            }
                        }
                    ]
                },
                "SSESpecification": {
                    "SSEEnabled": true
                },
                "PointInTimeRecoverySpecification": {
                    "Fn::If": [
                        "ShouldUsePointInTimeRecovery",
                        {
                            "PointInTimeRecoveryEnabled": true
                        },
                        {
                            "Ref": "AWS::NoValue"
                        }
                    ]
                }
            }
        },
        "AccountIAMRole": {
            "Type": "AWS::IAM::Role",
            "Properties": {
                "RoleName": {
                    "Fn::If": [
                        "HasEnvironmentParameter",
                        {
                            "Fn::Join": [
                                "-",
                                [
                                    "Account",
                                    "role",
                                    {
                                        "Ref": "GetAttGraphQLAPIApiId"
                                    },
                                    {
                                        "Ref": "env"
                                    }
                                ]
                            ]
                        },
                        {
                            "Fn::Join": [
                                "-",
                                [
                                    "Account",
                                    "role",
                                    {
                                        "Ref": "GetAttGraphQLAPIApiId"
                                    }
                                ]
                            ]
                        }
                    ]
                },
                "AssumeRolePolicyDocument": {
                    "Version": "2012-10-17",
                    "Statement": [
                        {
                            "Effect": "Allow",
                            "Principal": {
                                "Service": "appsync.amazonaws.com"
                            },
                            "Action": "sts:AssumeRole"
                        }
                    ]
                },
                "Policies": [
                    {
                        "PolicyName": "DynamoDBAccess",
                        "PolicyDocument": {
                            "Version": "2012-10-17",
                            "Statement": [
                                {
                                    "Effect": "Allow",
                                    "Action": [
                                        "dynamodb:BatchGetItem",
                                        "dynamodb:BatchWriteItem",
                                        "dynamodb:PutItem",
                                        "dynamodb:DeleteItem",
                                        "dynamodb:GetItem",
                                        "dynamodb:Scan",
                                        "dynamodb:Query",
                                        "dynamodb:UpdateItem"
                                    ],
                                    "Resource": [
                                        {
                                            "Fn::Sub": [
                                                "arn:aws:dynamodb:${AWS::Region}:${AWS::AccountId}:table/${tablename}",
                                                {
                                                    "tablename": {
                                                        "Fn::If": [
                                                            "HasEnvironmentParameter",
                                                            {
                                                                "Fn::Join": [
                                                                    "-",
                                                                    [
                                                                        "Account",
                                                                        {
                                                                            "Ref": "GetAttGraphQLAPIApiId"
                                                                        },
                                                                        {
                                                                            "Ref": "env"
                                                                        }
                                                                    ]
                                                                ]
                                                            },
                                                            {
                                                                "Fn::Join": [
                                                                    "-",
                                                                    [
                                                                        "Account",
                                                                        {
                                                                            "Ref": "GetAttGraphQLAPIApiId"
                                                                        }
                                                                    ]
                                                                ]
                                                            }
                                                        ]
                                                    }
                                                }
                                            ]
                                        },
                                        {
                                            "Fn::Sub": [
                                                "arn:aws:dynamodb:${AWS::Region}:${AWS::AccountId}:table/${tablename}/*",
                                                {
                                                    "tablename": {
                                                        "Fn::If": [
                                                            "HasEnvironmentParameter",
                                                            {
                                                                "Fn::Join": [
                                                                    "-",
                                                                    [
                                                                        "Account",
                                                                        {
                                                                            "Ref": "GetAttGraphQLAPIApiId"
                                                                        },
                                                                        {
                                                                            "Ref": "env"
                                                                        }
                                                                    ]
                                                                ]
                                                            },
                                                            {
                                                                "Fn::Join": [
                                                                    "-",
                                                                    [
                                                                        "Account",
                                                                        {
                                                                            "Ref": "GetAttGraphQLAPIApiId"
                                                                        }
                                                                    ]
                                                                ]
                                                            }
                                                        ]
                                                    }
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        }
                    }
                ]
            }
        },
        "AccountDataSource": {
            "Type": "AWS::AppSync::DataSource",
            "Properties": {
                "ApiId": {
                    "Ref": "GetAttGraphQLAPIApiId"
                },
                "Name": "AccountTable",
                "Type": "AMAZON_DYNAMODB",
                "ServiceRoleArn": {
                    "Fn::GetAtt": [
                        "AccountIAMRole",
                        "Arn"
                    ]
                },
                "DynamoDBConfig": {
                    "AwsRegion": {
                        "Ref": "AWS::Region"
                    },
                    "TableName": {
                        "Fn::If": [
                            "HasEnvironmentParameter",
                            {
                                "Fn::Join": [
                                    "-",
                                    [
                                        "Account",
                                        {
                                            "Ref": "GetAttGraphQLAPIApiId"
                                        },
                                        {
                                            "Ref": "env"
                                        }
                                    ]
                                ]
                            },
                            {
                                "Fn::Join": [
                                    "-",
                                    [
                                        "Account",
                                        {
                                            "Ref": "GetAttGraphQLAPIApiId"
                                        }
                                    ]
                                ]
                            }
                        ]
                    }
                }
            },
            "DependsOn": [
                "AccountIAMRole"
            ]
        },
        "GetAccountResolver": {
            "Type": "AWS::AppSync::Resolver",
            "Properties": {
                "ApiId": {
                    "Ref": "GetAttGraphQLAPIApiId"
                },
                "DataSourceName": {
                    "Fn::GetAtt": [
                        "AccountDataSource",
                        "Name"
                    ]
                },
                "FieldName": "getAccount",
                "TypeName": "Query",
                "RequestMappingTemplateS3Location": {
                    "Fn::Sub": [
                        "s3://${S3DeploymentBucket}/${S3DeploymentRootKey}/resolvers/${ResolverFileName}",
                        {
                            "S3DeploymentBucket": {
                                "Ref": "S3DeploymentBucket"
                            },
                            "S3DeploymentRootKey": {
                                "Ref": "S3DeploymentRootKey"
                            },
                            "ResolverFileName": {
                                "Fn::Join": [
                                    ".",
                                    [
                                        "Query",
                                        "getAccount",
                                        "req",
                                        "vtl"
                                    ]
                                ]
                            }
                        }
                    ]
                },
                "ResponseMappingTemplateS3Location": {
                    "Fn::Sub": [
                        "s3://${S3DeploymentBucket}/${S3DeploymentRootKey}/resolvers/${ResolverFileName}",
                        {
                            "S3DeploymentBucket": {
                                "Ref": "S3DeploymentBucket"
                            },
                            "S3DeploymentRootKey": {
                                "Ref": "S3DeploymentRootKey"
                            },
                            "ResolverFileName": {
                                "Fn::Join": [
                                    ".",
                                    [
                                        "Query",
                                        "getAccount",
                                        "res",
                                        "vtl"
                                    ]
                                ]
                            }
                        }
                    ]
                }
            }
        },
        "ListAccountResolver": {
            "Type": "AWS::AppSync::Resolver",
            "Properties": {
                "ApiId": {
                    "Ref": "GetAttGraphQLAPIApiId"
                },
                "DataSourceName": {
                    "Fn::GetAtt": [
                        "AccountDataSource",
                        "Name"
                    ]
                },
                "FieldName": "listAccounts",
                "TypeName": "Query",
                "RequestMappingTemplateS3Location": {
                    "Fn::Sub": [
                        "s3://${S3DeploymentBucket}/${S3DeploymentRootKey}/resolvers/${ResolverFileName}",
                        {
                            "S3DeploymentBucket": {
                                "Ref": "S3DeploymentBucket"
                            },
                            "S3DeploymentRootKey": {
                                "Ref": "S3DeploymentRootKey"
                            },
                            "ResolverFileName": {
                                "Fn::Join": [
                                    ".",
                                    [
                                        "Query",
                                        "listAccounts",
                                        "req",
                                        "vtl"
                                    ]
                                ]
                            }
                        }
                    ]
                },
                "ResponseMappingTemplateS3Location": {
                    "Fn::Sub": [
                        "s3://${S3DeploymentBucket}/${S3DeploymentRootKey}/resolvers/${ResolverFileName}",
                        {
                            "S3DeploymentBucket": {
                                "Ref": "S3DeploymentBucket"
                            },
                            "S3DeploymentRootKey": {
                                "Ref": "S3DeploymentRootKey"
                            },
                            "ResolverFileName": {
                                "Fn::Join": [
                                    ".",
                                    [
                                        "Query",
                                        "listAccounts",
                                        "res",
                                        "vtl"
                                    ]
                                ]
                            }
                        }
                    ]
                }
            }
        },
        "CreateAccountResolver": {
            "Type": "AWS::AppSync::Resolver",
            "Properties": {
                "ApiId": {
                    "Ref": "GetAttGraphQLAPIApiId"
                },
                "DataSourceName": {
                    "Fn::GetAtt": [
                        "AccountDataSource",
                        "Name"
                    ]
                },
                "FieldName": "createAccount",
                "TypeName": "Mutation",
                "RequestMappingTemplateS3Location": {
                    "Fn::Sub": [
                        "s3://${S3DeploymentBucket}/${S3DeploymentRootKey}/resolvers/${ResolverFileName}",
                        {
                            "S3DeploymentBucket": {
                                "Ref": "S3DeploymentBucket"
                            },
                            "S3DeploymentRootKey": {
                                "Ref": "S3DeploymentRootKey"
                            },
                            "ResolverFileName": {
                                "Fn::Join": [
                                    ".",
                                    [
                                        "Mutation",
                                        "createAccount",
                                        "req",
                                        "vtl"
                                    ]
                                ]
                            }
                        }
                    ]
                },
                "ResponseMappingTemplateS3Location": {
                    "Fn::Sub": [
                        "s3://${S3DeploymentBucket}/${S3DeploymentRootKey}/resolvers/${ResolverFileName}",
                        {
                            "S3DeploymentBucket": {
                                "Ref": "S3DeploymentBucket"
                            },
                            "S3DeploymentRootKey": {
                                "Ref": "S3DeploymentRootKey"
                            },
                            "ResolverFileName": {
                                "Fn::Join": [
                                    ".",
                                    [
                                        "Mutation",
                                        "createAccount",
                                        "res",
                                        "vtl"
                                    ]
                                ]
                            }
                        }
                    ]
                }
            }
        },
        "UpdateAccountResolver": {
            "Type": "AWS::AppSync::Resolver",
            "Properties": {
                "ApiId": {
                    "Ref": "GetAttGraphQLAPIApiId"
                },
                "DataSourceName": {
                    "Fn::GetAtt": [
                        "AccountDataSource",
                        "Name"
                    ]
                },
                "FieldName": "updateAccount",
                "TypeName": "Mutation",
                "RequestMappingTemplateS3Location": {
                    "Fn::Sub": [
                        "s3://${S3DeploymentBucket}/${S3DeploymentRootKey}/resolvers/${ResolverFileName}",
                        {
                            "S3DeploymentBucket": {
                                "Ref": "S3DeploymentBucket"
                            },
                            "S3DeploymentRootKey": {
                                "Ref": "S3DeploymentRootKey"
                            },
                            "ResolverFileName": {
                                "Fn::Join": [
                                    ".",
                                    [
                                        "Mutation",
                                        "updateAccount",
                                        "req",
                                        "vtl"
                                    ]
                                ]
                            }
                        }
                    ]
                },
                "ResponseMappingTemplateS3Location": {
                    "Fn::Sub": [
                        "s3://${S3DeploymentBucket}/${S3DeploymentRootKey}/resolvers/${ResolverFileName}",
                        {
                            "S3DeploymentBucket": {
                                "Ref": "S3DeploymentBucket"
                            },
                            "S3DeploymentRootKey": {
                                "Ref": "S3DeploymentRootKey"
                            },
                            "ResolverFileName": {
                                "Fn::Join": [
                                    ".",
                                    [
                                        "Mutation",
                                        "updateAccount",
                                        "res",
                                        "vtl"
                                    ]
                                ]
                            }
                        }
                    ]
                }
            }
        },
        "DeleteAccountResolver": {
            "Type": "AWS::AppSync::Resolver",
            "Properties": {
                "ApiId": {
                    "Ref": "GetAttGraphQLAPIApiId"
                },
                "DataSourceName": {
                    "Fn::GetAtt": [
                        "AccountDataSource",
                        "Name"
                    ]
                },
                "FieldName": "deleteAccount",
                "TypeName": "Mutation",
                "RequestMappingTemplateS3Location": {
                    "Fn::Sub": [
                        "s3://${S3DeploymentBucket}/${S3DeploymentRootKey}/resolvers/${ResolverFileName}",
                        {
                            "S3DeploymentBucket": {
                                "Ref": "S3DeploymentBucket"
                            },
                            "S3DeploymentRootKey": {
                                "Ref": "S3DeploymentRootKey"
                            },
                            "ResolverFileName": {
                                "Fn::Join": [
                                    ".",
                                    [
                                        "Mutation",
                                        "deleteAccount",
                                        "req",
                                        "vtl"
                                    ]
                                ]
                            }
                        }
                    ]
                },
                "ResponseMappingTemplateS3Location": {
                    "Fn::Sub": [
                        "s3://${S3DeploymentBucket}/${S3DeploymentRootKey}/resolvers/${ResolverFileName}",
                        {
                            "S3DeploymentBucket": {
                                "Ref": "S3DeploymentBucket"
                            },
                            "S3DeploymentRootKey": {
                                "Ref": "S3DeploymentRootKey"
                            },
                            "ResolverFileName": {
                                "Fn::Join": [
                                    ".",
                                    [
                                        "Mutation",
                                        "deleteAccount",
                                        "res",
                                        "vtl"
                                    ]
                                ]
                            }
                        }
                    ]
                }
            }
        }
    },
    "Outputs": {
        "GetAttAccountTableStreamArn": {
            "Description": "Your DynamoDB table StreamArn.",
            "Value": {
                "Fn::GetAtt": [
                    "AccountTable",
                    "StreamArn"
                ]
            },
            "Export": {
                "Name": {
                    "Fn::Join": [
                        ":",
                        [
                            {
                                "Ref": "AppSyncApiId"
                            },
                            "GetAtt",
                            "AccountTable",
                            "StreamArn"
                        ]
                    ]
                }
            }
        },
        "GetAttAccountDataSourceName": {
            "Description": "Your model DataSource name.",
            "Value": {
                "Fn::GetAtt": [
                    "AccountDataSource",
                    "Name"
                ]
            },
            "Export": {
                "Name": {
                    "Fn::Join": [
                        ":",
                        [
                            {
                                "Ref": "AppSyncApiId"
                            },
                            "GetAtt",
                            "AccountDataSource",
                            "Name"
                        ]
                    ]
                }
            }
        },
        "GetAttAccountTableName": {
            "Description": "Your DynamoDB table name.",
            "Value": {
                "Ref": "AccountTable"
            },
            "Export": {
                "Name": {
                    "Fn::Join": [
                        ":",
                        [
                            {
                                "Ref": "AppSyncApiId"
                            },
                            "GetAtt",
                            "AccountTable",
                            "Name"
                        ]
                    ]
                }
            }
        }
    },
    "Conditions": {
        "ShouldUsePayPerRequestBilling": {
            "Fn::Equals": [
                {
                    "Ref": "DynamoDBBillingMode"
                },
                "PAY_PER_REQUEST"
            ]
        },
        "ShouldUsePointInTimeRecovery": {
            "Fn::Equals": [
                {
                    "Ref": "DynamoDBEnablePointInTimeRecovery"
                },
                "true"
            ]
        },
        "ShouldCreateAPIKey": {
            "Fn::And": [
                {
                    "Fn::Not": [
                        {
                            "Fn::Equals": [
                                {
                                    "Ref": "APIKeyExpirationEpoch"
                                },
                                -1
                            ]
                        }
                    ]
                },
                {
                    "Fn::Equals": [
                        {
                            "Ref": "AuthCognitoUserPoolId"
                        },
                        "NONE"
                    ]
                }
            ]
        },
        "APIKeyExpirationEpochIsPositive": {
            "Fn::And": [
                {
                    "Fn::Not": [
                        {
                            "Fn::Equals": [
                                {
                                    "Ref": "APIKeyExpirationEpoch"
                                },
                                -1
                            ]
                        }
                    ]
                },
                {
                    "Fn::Not": [
                        {
                            "Fn::Equals": [
                                {
                                    "Ref": "APIKeyExpirationEpoch"
                                },
                                0
                            ]
                        }
                    ]
                }
            ]
        },
        "HasEnvironmentParameter": {
            "Fn::Not": [
                {
                    "Fn::Equals": [
                        {
                            "Ref": "env"
                        },
                        "NONE"
                    ]
                }
            ]
        }
    }
}