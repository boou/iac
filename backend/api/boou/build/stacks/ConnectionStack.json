{
    "AWSTemplateFormatVersion": "2010-09-09",
    "Description": "An auto-generated nested stack.",
    "Metadata": {},
    "Parameters": {
        "AppSyncApiName": {
            "Type": "String",
            "Description": "The name of the AppSync API",
            "Default": "AppSyncSimpleTransform"
        },
        "APIKeyExpirationEpoch": {
            "Type": "Number",
            "Description": "The epoch time in seconds when the API Key should expire. Setting this to 0 will default to 1 week from the deployment date. Setting this to -1 will not create an API Key.",
            "Default": 0
        },
        "DynamoDBModelTableReadIOPS": {
            "Type": "Number",
            "Description": "The number of read IOPS the table should support.",
            "Default": 5
        },
        "DynamoDBModelTableWriteIOPS": {
            "Type": "Number",
            "Description": "The number of write IOPS the table should support.",
            "Default": 5
        },
        "DynamoDBBillingMode": {
            "Type": "String",
            "Description": "Configure @model types to create DynamoDB tables with PAY_PER_REQUEST or PROVISIONED billing modes.",
            "Default": "PAY_PER_REQUEST",
            "AllowedValues": [
                "PAY_PER_REQUEST",
                "PROVISIONED"
            ]
        },
        "DynamoDBEnablePointInTimeRecovery": {
            "Type": "String",
            "Description": "Whether to enable Point in Time Recovery on the table",
            "Default": "false",
            "AllowedValues": [
                "true",
                "false"
            ]
        },
        "AuthCognitoUserPoolId": {
            "Type": "String",
            "Description": "The id of an existing User Pool to connect. If this is changed, a user pool will not be created for you.",
            "Default": "NONE"
        },
        "env": {
            "Type": "String",
            "Description": "The environment name. e.g. Dev, Test, or Production",
            "Default": "NONE"
        },
        "S3DeploymentBucket": {
            "Type": "String",
            "Description": "The S3 bucket containing all deployment assets for the project."
        },
        "S3DeploymentRootKey": {
            "Type": "String",
            "Description": "An S3 key relative to the S3DeploymentBucket that points to the root of the deployment directory."
        },
        "AppSyncApiId": {
            "Type": "String",
            "Description": "The id of the AppSync API associated with this project."
        },
        "GetAttGraphQLAPIApiId": {
            "Type": "String",
            "Description": "Auto-generated parameter that forwards Fn.GetAtt(GraphQLAPI, ApiId) through to nested stacks."
        }
    },
    "Resources": {
        "AccountcreditsResolver": {
            "Type": "AWS::AppSync::Resolver",
            "Properties": {
                "ApiId": {
                    "Ref": "GetAttGraphQLAPIApiId"
                },
                "DataSourceName": {
                    "Fn::ImportValue": {
                        "Fn::Join": [
                            ":",
                            [
                                {
                                    "Ref": "AppSyncApiId"
                                },
                                "GetAtt",
                                "CreditDataSource",
                                "Name"
                            ]
                        ]
                    }
                },
                "FieldName": "credits",
                "TypeName": "Account",
                "RequestMappingTemplateS3Location": {
                    "Fn::Sub": [
                        "s3://${S3DeploymentBucket}/${S3DeploymentRootKey}/resolvers/${ResolverFileName}",
                        {
                            "S3DeploymentBucket": {
                                "Ref": "S3DeploymentBucket"
                            },
                            "S3DeploymentRootKey": {
                                "Ref": "S3DeploymentRootKey"
                            },
                            "ResolverFileName": {
                                "Fn::Join": [
                                    ".",
                                    [
                                        "Account",
                                        "credits",
                                        "req",
                                        "vtl"
                                    ]
                                ]
                            }
                        }
                    ]
                },
                "ResponseMappingTemplateS3Location": {
                    "Fn::Sub": [
                        "s3://${S3DeploymentBucket}/${S3DeploymentRootKey}/resolvers/${ResolverFileName}",
                        {
                            "S3DeploymentBucket": {
                                "Ref": "S3DeploymentBucket"
                            },
                            "S3DeploymentRootKey": {
                                "Ref": "S3DeploymentRootKey"
                            },
                            "ResolverFileName": {
                                "Fn::Join": [
                                    ".",
                                    [
                                        "Account",
                                        "credits",
                                        "res",
                                        "vtl"
                                    ]
                                ]
                            }
                        }
                    ]
                }
            }
        },
        "AccountloanResolver": {
            "Type": "AWS::AppSync::Resolver",
            "Properties": {
                "ApiId": {
                    "Ref": "GetAttGraphQLAPIApiId"
                },
                "DataSourceName": {
                    "Fn::ImportValue": {
                        "Fn::Join": [
                            ":",
                            [
                                {
                                    "Ref": "AppSyncApiId"
                                },
                                "GetAtt",
                                "LoanDataSource",
                                "Name"
                            ]
                        ]
                    }
                },
                "FieldName": "loan",
                "TypeName": "Account",
                "RequestMappingTemplateS3Location": {
                    "Fn::Sub": [
                        "s3://${S3DeploymentBucket}/${S3DeploymentRootKey}/resolvers/${ResolverFileName}",
                        {
                            "S3DeploymentBucket": {
                                "Ref": "S3DeploymentBucket"
                            },
                            "S3DeploymentRootKey": {
                                "Ref": "S3DeploymentRootKey"
                            },
                            "ResolverFileName": {
                                "Fn::Join": [
                                    ".",
                                    [
                                        "Account",
                                        "loan",
                                        "req",
                                        "vtl"
                                    ]
                                ]
                            }
                        }
                    ]
                },
                "ResponseMappingTemplateS3Location": {
                    "Fn::Sub": [
                        "s3://${S3DeploymentBucket}/${S3DeploymentRootKey}/resolvers/${ResolverFileName}",
                        {
                            "S3DeploymentBucket": {
                                "Ref": "S3DeploymentBucket"
                            },
                            "S3DeploymentRootKey": {
                                "Ref": "S3DeploymentRootKey"
                            },
                            "ResolverFileName": {
                                "Fn::Join": [
                                    ".",
                                    [
                                        "Account",
                                        "loan",
                                        "res",
                                        "vtl"
                                    ]
                                ]
                            }
                        }
                    ]
                }
            }
        },
        "CreditloanResolver": {
            "Type": "AWS::AppSync::Resolver",
            "Properties": {
                "ApiId": {
                    "Ref": "GetAttGraphQLAPIApiId"
                },
                "DataSourceName": {
                    "Fn::ImportValue": {
                        "Fn::Join": [
                            ":",
                            [
                                {
                                    "Ref": "AppSyncApiId"
                                },
                                "GetAtt",
                                "LoanDataSource",
                                "Name"
                            ]
                        ]
                    }
                },
                "FieldName": "loan",
                "TypeName": "Credit",
                "RequestMappingTemplateS3Location": {
                    "Fn::Sub": [
                        "s3://${S3DeploymentBucket}/${S3DeploymentRootKey}/resolvers/${ResolverFileName}",
                        {
                            "S3DeploymentBucket": {
                                "Ref": "S3DeploymentBucket"
                            },
                            "S3DeploymentRootKey": {
                                "Ref": "S3DeploymentRootKey"
                            },
                            "ResolverFileName": {
                                "Fn::Join": [
                                    ".",
                                    [
                                        "Credit",
                                        "loan",
                                        "req",
                                        "vtl"
                                    ]
                                ]
                            }
                        }
                    ]
                },
                "ResponseMappingTemplateS3Location": {
                    "Fn::Sub": [
                        "s3://${S3DeploymentBucket}/${S3DeploymentRootKey}/resolvers/${ResolverFileName}",
                        {
                            "S3DeploymentBucket": {
                                "Ref": "S3DeploymentBucket"
                            },
                            "S3DeploymentRootKey": {
                                "Ref": "S3DeploymentRootKey"
                            },
                            "ResolverFileName": {
                                "Fn::Join": [
                                    ".",
                                    [
                                        "Credit",
                                        "loan",
                                        "res",
                                        "vtl"
                                    ]
                                ]
                            }
                        }
                    ]
                }
            }
        },
        "LoanbankResolver": {
            "Type": "AWS::AppSync::Resolver",
            "Properties": {
                "ApiId": {
                    "Ref": "GetAttGraphQLAPIApiId"
                },
                "DataSourceName": {
                    "Fn::ImportValue": {
                        "Fn::Join": [
                            ":",
                            [
                                {
                                    "Ref": "AppSyncApiId"
                                },
                                "GetAtt",
                                "BankDataSource",
                                "Name"
                            ]
                        ]
                    }
                },
                "FieldName": "bank",
                "TypeName": "Loan",
                "RequestMappingTemplateS3Location": {
                    "Fn::Sub": [
                        "s3://${S3DeploymentBucket}/${S3DeploymentRootKey}/resolvers/${ResolverFileName}",
                        {
                            "S3DeploymentBucket": {
                                "Ref": "S3DeploymentBucket"
                            },
                            "S3DeploymentRootKey": {
                                "Ref": "S3DeploymentRootKey"
                            },
                            "ResolverFileName": {
                                "Fn::Join": [
                                    ".",
                                    [
                                        "Loan",
                                        "bank",
                                        "req",
                                        "vtl"
                                    ]
                                ]
                            }
                        }
                    ]
                },
                "ResponseMappingTemplateS3Location": {
                    "Fn::Sub": [
                        "s3://${S3DeploymentBucket}/${S3DeploymentRootKey}/resolvers/${ResolverFileName}",
                        {
                            "S3DeploymentBucket": {
                                "Ref": "S3DeploymentBucket"
                            },
                            "S3DeploymentRootKey": {
                                "Ref": "S3DeploymentRootKey"
                            },
                            "ResolverFileName": {
                                "Fn::Join": [
                                    ".",
                                    [
                                        "Loan",
                                        "bank",
                                        "res",
                                        "vtl"
                                    ]
                                ]
                            }
                        }
                    ]
                }
            }
        }
    },
    "Outputs": {},
    "Conditions": {
        "ShouldUsePayPerRequestBilling": {
            "Fn::Equals": [
                {
                    "Ref": "DynamoDBBillingMode"
                },
                "PAY_PER_REQUEST"
            ]
        },
        "ShouldUsePointInTimeRecovery": {
            "Fn::Equals": [
                {
                    "Ref": "DynamoDBEnablePointInTimeRecovery"
                },
                "true"
            ]
        },
        "ShouldCreateAPIKey": {
            "Fn::And": [
                {
                    "Fn::Not": [
                        {
                            "Fn::Equals": [
                                {
                                    "Ref": "APIKeyExpirationEpoch"
                                },
                                -1
                            ]
                        }
                    ]
                },
                {
                    "Fn::Equals": [
                        {
                            "Ref": "AuthCognitoUserPoolId"
                        },
                        "NONE"
                    ]
                }
            ]
        },
        "APIKeyExpirationEpochIsPositive": {
            "Fn::And": [
                {
                    "Fn::Not": [
                        {
                            "Fn::Equals": [
                                {
                                    "Ref": "APIKeyExpirationEpoch"
                                },
                                -1
                            ]
                        }
                    ]
                },
                {
                    "Fn::Not": [
                        {
                            "Fn::Equals": [
                                {
                                    "Ref": "APIKeyExpirationEpoch"
                                },
                                0
                            ]
                        }
                    ]
                }
            ]
        },
        "HasEnvironmentParameter": {
            "Fn::Not": [
                {
                    "Fn::Equals": [
                        {
                            "Ref": "env"
                        },
                        "NONE"
                    ]
                }
            ]
        }
    }
}